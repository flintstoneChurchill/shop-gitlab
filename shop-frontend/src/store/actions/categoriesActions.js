import axiosApi from "../../axiosApi";
import {FETCH_CATEGORIES_SUCCESS} from "../actionTypes";

const fetchCategoriesSuccess = categories => {
  return {type: FETCH_CATEGORIES_SUCCESS, categories};
};
export const fetchCategories = () => {
  return async dispatch => {
    const response = await axiosApi.get("/categories");
    dispatch(fetchCategoriesSuccess(response.data));
  };
};
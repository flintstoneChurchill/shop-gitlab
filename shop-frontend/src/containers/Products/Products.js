import React, {useEffect} from "react";
import {Grid} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductItem from "../../components/ProductItem/ProductItem";

const Products = () => {
  const dispatch = useDispatch();
  const products = useSelector(state => state.products.products);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant="h4">
            Products
          </Typography>
        </Grid>
        {
          user && user.role === "admin" && <Grid item>
            <Button color="primary" component={Link} to="/products/new">
              Add product
            </Button>
          </Grid>
        }
      </Grid>
      <h2>Building at {(new Date()).toISOString()}</h2>
      <Grid item container direction="row" spacing={2}>
        {products.map(product => {
          return <ProductItem
            key={product._id}
            id={product._id}
            price={product.price}
            title={product.title}
            image={product.image}
            category={product.category}
          />
        })}
      </Grid>
    </Grid>
  );
};

export default Products;
const config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3010',
      show: false,
      host: "chrome-service",
      windowSize: '1920x1080',
      waitForNavigation: "networkidle0",
      args: [
        '--disable-gpu',
        '--headless',
        '--disable-dev-shm-usage',
        '--disable-setuid-sandbox',
        '--no-first-run',
        '--no-sandbox',
        '--no-zygote',
        '--single-process',
      ]
    },
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    }
  },
  name: 'shop-tests'
}

config.helpers.Puppeteer.host =
  process.env.SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR;
// Reset config because it got overriden by BrowserStack auto detect mechanism
config.helpers.Puppeteer.protocol = "http";
config.helpers.Puppeteer.port = 4444;


exports.config = config;

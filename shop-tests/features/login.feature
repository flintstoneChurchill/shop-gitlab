# language: ru

Функция: Логин пользователя
  Находясь на сайте под анонимным пользователем
  Пытаюсь войти на сайт через форму входа

  Сценарий: Успешный вход
    Допустим я нахожусь на странице "/login"
    Если я ввожу "user" в поле "username"
    И я ввожу "1@345qWert" в поле "password"
    И я нажимаю на кнопку "loginBtn"
    То я вижу текст "HELLO, USER"
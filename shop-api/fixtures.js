const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");
const Category = require("./models/Category");
const User = require("./models/User");
const Product = require("./models/Product");

mongoose.connect(config.db.url + "/" + config.db.name, { useNewUrlParser: true });

const db = mongoose.connection;

db.once("open", async () => {
  try {
    await db.dropCollection("categories");
    await db.dropCollection("products");
    await db.dropCollection("users");
  } catch(e) {
    console.log("Collection were not presented, skipping drop...");
  }

  const [cpuCategory, hddCategory] = await Category.create({
    title: "CPUs",
    description: "Central Processor Units"
  }, {
    title: "HDDs",
    description: "Hard Disk Drives"
  });

  await Product.create({
    title: "Intel core i7",
    price: 300,
    category: cpuCategory._id,
    image: "cpu.jpg"
  }, {
    title: "Seagate BarraCuda 1TB",
    price: 150,
    category: hddCategory._id,
    image: "hdd.png"
  });

  await User.create({
    username: "user",
    email: "user@shop.com",
    password: "1@345qWert",
    token: nanoid(),
    role: "user"
  }, {
    username: "admin",
    email: "admin@shop.com",
    password: "1@345qWert",
    token: nanoid(),
    role: "admin"
  });

  db.close();
});
const router = require("express").Router();
const User = require("../models/User");

router.ws("/", async (ws, req) => {
  const token = req.query.token;
  console.log(req.query.token);
  const user = await User.findOne({token});

  if (!user) {
    return ws.send(JSON.stringify({
      type: "ERROR",
      message: {
        text: "Сначала залогиньтесь"
      }
    }));
  }

  ws.send(JSON.stringify({
    type: "INIT",
    message: {
      text: "Привет, " + user.username + ", Вас приветствует Понятливый Олег!"
    }
  }));

});

module.exports = router;
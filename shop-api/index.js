const mongoose = require("mongoose");
const {app, port} = require("./app");
const config = require("./config");

const run = async () => {
  await mongoose.connect(config.db.url + "/" + config.db.name, { useNewUrlParser: true, autoIndex: true });
  console.log("Mongoose connected");
  app.listen(port, () => {
    console.log("Server started");
  });
};

run().catch(console.log);